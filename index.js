
// Bài 1: tính tiền lương nhân viên

function tinhTienLuong() {
    var soNgayLamEl = document.getElementById("so-ngay-lam").value * 1;
    var tienLuong = soNgayLamEl * 100000;
    document.getElementById("tienLuong").innerHTML = `Số tiền lương của bạn là: ${new Intl.NumberFormat().format(tienLuong)} VND`;
}

 // bài 2: tính giá trị trung bình

function tinhTrungBinh() {
    var number1El = document.getElementById("number1").value * 1;
    var number2El = document.getElementById("number2").value * 1;
    var number3El = document.getElementById("number3").value * 1;
    var number4El = document.getElementById("number4").value * 1;
    var number5El = document.getElementById("number5").value * 1;

    var stb = (number1El + number2El + number3El + number4El + number5El) / 5;

    document.getElementById("so-trung-binh").innerHTML = `Số trung bình là: ${stb.toFixed(2)}`;
}

 // bài 3: QUy đổi tiền
 
function quyDoiTien() {
    var soTienUsdEl = document.getElementById("so-tien-usd").value * 1;
    var soTienVnd = soTienUsdEl * 23500;

    document.getElementById("so-tien-vnd").innerHTML = ` Số tiền: ${new Intl.NumberFormat().format(soTienVnd)} VND`;
}
 
 // bải 4: tính diện tích chu vi hình chữ nhật
 
function chuNhat() {
    var chieuDaiEl = document.getElementById("chieuDai").value * 1;
    var chieuRongEl = document.getElementById("chieuRong").value * 1;

    var chuVi = (chieuDaiEl + chieuRongEl) * 2;
    var dienTich = chieuDaiEl * chieuRongEl;

    document.getElementById("chuVi").innerHTML = `Chu Vi = ${chuVi}`;
    document.getElementById("dienTich").innerHTML = `Diện Tích = ${dienTich}`;
}
 
 // bài 5: tính tổng 2 ký số
 
function tinhTong() {
    var soCoHaiChuSoEl = document.getElementById("so-hai-chu-so").value * 1;
    var soHangDonVi = soCoHaiChuSoEl % 10;
    var soHangChuc = Math.floor(soCoHaiChuSoEl / 10);

    var tongHaiKySo = soHangChuc + soHangDonVi;
    document.getElementById("tong-hai-ky-so").innerHTML = `Tổng hai ký số là: ${tongHaiKySo}`;
}
 
 
 